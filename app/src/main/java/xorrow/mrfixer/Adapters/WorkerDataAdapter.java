package xorrow.mrfixer.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;
import java.util.Random;

import xorrow.mrfixer.R;
import xorrow.mrfixer.DataModels.WorkerDataModel;

/**
 * Created by Asad on 18-Jan-17.
 */

public class WorkerDataAdapter extends BaseQuickAdapter<WorkerDataModel, BaseViewHolder> {
    Context context;

    public WorkerDataAdapter(int layoutResId, List<WorkerDataModel> data, Context context) {
        super(layoutResId, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, WorkerDataModel workerDataModel) {
        TextView name;
        TextView profession;
        TextView distance;
        CircularImageView image;
        TextView ratings;

        name = baseViewHolder.getView(R.id.worker_name);
        profession = baseViewHolder.getView(R.id.worker_profession);
        distance = baseViewHolder.getView(R.id.distance);
        image = baseViewHolder.getView(R.id.image);
        ratings = baseViewHolder.getView(R.id.starRatings);

        name.setText(workerDataModel.getName() != null ? workerDataModel.getName() : "");
        profession.setText(workerDataModel.getProfession() != null ? workerDataModel.getProfession() : "");
        distance.setText(String.valueOf(workerDataModel.getDistance().intValue()));
        Drawable workerImage;
        if (workerDataModel.getProfession().toLowerCase()
                .contains("plumber")) {
            workerImage = ContextCompat.getDrawable(context, R.drawable.screwhomemap);
        } else if (workerDataModel.getProfession().toLowerCase()
                .contains("electrician")) {
            workerImage = ContextCompat.getDrawable(context, R.drawable.bolthome);
        } else if (workerDataModel.getProfession().toLowerCase()
                .contains("carpanter")) {
            workerImage = ContextCompat.getDrawable(context, R.drawable.sawmap);
        } else if (workerDataModel.getProfession().toLowerCase()
                .contains("mechanic")) {
            workerImage = ContextCompat.getDrawable(context, R.drawable.mechanichome);
        } else
            workerImage = ContextCompat.getDrawable(context, R.drawable.screwhomemap);

        image.setImageDrawable(workerImage);

        Random random = new Random();
        ratings.setText(String.valueOf(random.nextInt(10 - 1 + 1) + 1));

    }





}
