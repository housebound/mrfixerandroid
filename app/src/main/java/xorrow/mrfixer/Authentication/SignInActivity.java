package xorrow.mrfixer.Authentication;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import xorrow.mrfixer.Activities.MainActivity;
import xorrow.mrfixer.R;

import static xorrow.mrfixer.Application.ApplicationClass.mAuth;

public class SignInActivity extends AppCompatActivity {
    private static final String TAG = "FacebookAuthresult";
    EditText email;
    EditText password;
    Button btn_login;
    Button btn_fgtPass;
    Button fb;
    ProgressDialog progress;
    CallbackManager mCallbackManager;
    Button test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
//        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        email = (EditText) findViewById(xorrow.mrfixer.R.id.input_email);
        password = (EditText) findViewById(xorrow.mrfixer.R.id.input_pw);
        btn_login = (Button) findViewById(xorrow.mrfixer.R.id.btn_login);
        btn_fgtPass = (Button) findViewById(xorrow.mrfixer.R.id.fgt_pw_btn);
        fb = (Button) findViewById(xorrow.mrfixer.R.id.btn_signupfb);
        progress = new ProgressDialog(this);
        progress.setMessage("Please wait.. ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "xorrow.mrfixer", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }


        final LoginButton loginButton = (LoginButton) findViewById(xorrow.mrfixer.R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });

        Dialog progressDialog = new Dialog(getApplicationContext());
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn(email.getText().toString().trim(), password.getText().toString());

            }
        });
        btn_fgtPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.callOnClick();
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (emailValidation() == 1 && !password.getText().toString().equals(""))
                    btn_login.setEnabled(true);
                else
                    btn_login.setEnabled(false);

            }
        };

        email.addTextChangedListener(textWatcher);
        password.addTextChangedListener(textWatcher);


    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());


                        if (task.isSuccessful()) {

                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(intent);
                            SignInActivity.this.finish();
                        } else {
                            Log.w("FacebookAuthresult", "signInWithCredential", task.getException());
                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }


                        // ...
                    }
                });
    }

    private int emailValidation() {

        String emailString = email.getText().toString();
        if (emailString.isEmpty()) {
            email.setError(null);
            return 0;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
            return -1;
        } else {
            email.setError(null);
            return 1;

        }

    }

    private void signIn(String email, String password) {

        progress.show();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                // If sign in fails, display a message to the user. If sign in succeeds
                // the auth state listener will be notified and logic to handle the
                // signed in user can be handled in the listener.
                if (!task.isSuccessful()) {
                    progress.dismiss();
                    Log.w(TAG, "signInWithEmail:failed", task.getException());
                    Toast.makeText(getApplicationContext(), task.getException()
                            .getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    progress.dismiss();
                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    startActivity(intent);
                    SignInActivity.this.finish();
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
