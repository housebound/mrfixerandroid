package xorrow.mrfixer.Authentication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import xorrow.mrfixer.R;

import static xorrow.mrfixer.Application.ApplicationClass.mAuth;

public class ForgotPasswordActivity extends AppCompatActivity {
    EditText emailAddress;
    Button forgetPasswordButton;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        emailAddress = (EditText) findViewById(xorrow.mrfixer.R.id.fgt_pw_email);

        forgetPasswordButton = (Button) findViewById(xorrow.mrfixer.R.id.btn_forgetpass);
        progress = new ProgressDialog(this);
        progress.setMessage("Please wait.. ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        forgetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                forgotPassword();
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!emailAddress.getText().toString().trim().equals("") && Patterns.EMAIL_ADDRESS.matcher(emailAddress.getText().toString().trim()).matches()) {
                    forgetPasswordButton.setEnabled(true);
                } else
                    forgetPasswordButton.setEnabled(false);

            }
        };

        emailAddress.addTextChangedListener(textWatcher);
    }

    private void forgotPassword() {
        progress.show();

        mAuth.sendPasswordResetEmail(emailAddress.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progress.dismiss();
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplication(), "An email has been sent with instructions on how to reset your password.", Toast.LENGTH_LONG).show();
                            finish();

                        }
                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

}
