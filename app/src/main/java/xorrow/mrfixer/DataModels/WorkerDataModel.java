package xorrow.mrfixer.DataModels;

import com.google.firebase.database.Exclude;

/**
 * Created by Asad on 18-Jan-17.
 */

public class WorkerDataModel {
    String name;
    String phoneNumber;
    String profession;
    Double distance;

    @Override

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkerDataModel that = (WorkerDataModel) o;

        if (!name.equals(that.name)) return false;
        if (!phoneNumber.equals(that.phoneNumber)) return false;
        return profession.equals(that.profession);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + phoneNumber.hashCode();
        result = 31 * result + profession.hashCode();
        return result;
    }

    public WorkerDataModel() {
    }

    public String getName() {
        return name.replace("\n", "").replace("\r", "");

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfession() {
        return profession.replace("\n", "").replace("\r", "");
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    @Exclude
    public Integer getDistance() {
        return distance.intValue();
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
