package xorrow.mrfixer.Intro;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;

import xorrow.mrfixer.Authentication.SignInActivity;
import xorrow.mrfixer.Intro.Intro1Fragment;
import xorrow.mrfixer.Intro.Intro2Fragment;
import xorrow.mrfixer.Intro.Intro3Fragment;
import xorrow.mrfixer.R;

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(new Intro1Fragment());
        addSlide(new Intro2Fragment());
        addSlide(new Intro3Fragment());
//        setFadeAnimation();
        showStatusBar(false);


        setColorDoneText(getResources().getColor(R.color.colorSecondaryText));
        setNextArrowColor(getResources().getColor(R.color.colorSecondaryText));
        setColorSkipButton(getResources().getColor(R.color.colorSecondaryText));
        setIndicatorColor(getResources().getColor(R.color.md_grey_700), getResources().getColor(R.color.md_grey_600));

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(IntroActivity.this, SignInActivity.class));
                IntroActivity.this.finish();

            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(IntroActivity.this, SignInActivity.class));
                IntroActivity.this.finish();

            }
        });

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
