package xorrow.mrfixer.Intro;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import xorrow.mrfixer.Authentication.SignInActivity;
import xorrow.mrfixer.Activities.MainActivity;
import xorrow.mrfixer.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


//                Intent WelcomeIntent = new Intent(SplashActivity.this,WelcomeActivity.class);
//                Intent Login = new Intent(SplashActivity.this,SelectionActivity.class);
//                Intent BaseIntent = new Intent(SplashActivity.this,BaseActivity.class);

//                SharedPreferences prefs = getApplicationContext().getSharedPreferences("HouseKeeping", Context.MODE_PRIVATE);
//                Boolean isLoggedIn = prefs.getBoolean("isLoggedIn", false);

                if (user != null) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    SplashActivity.this.finish();


                } else {
                    SharedPreferences settings = getSharedPreferences("MrFixer", MODE_PRIVATE);
                    boolean firstRun = settings.getBoolean("firstRun", true);

                    if (firstRun) {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, IntroActivity.class));
                        SplashActivity.this.finish();
                        SharedPreferences.Editor editor = settings.edit(); // Open the editor for our settings
                        editor.putBoolean("firstRun", false); // It is no longer the first run
                        editor.apply(); // Save all changed settings
                    } else {

                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                        SplashActivity.this.finish();

                    }

//                    overridePendingTransition(0,0);
//                    finish();
                }


            }
        }, 2500);
    }
}
