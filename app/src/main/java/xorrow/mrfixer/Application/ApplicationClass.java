package xorrow.mrfixer.Application;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static xorrow.mrfixer.Application.Constants.DEBUG_TAG;
import static xorrow.mrfixer.Utils.DataHandler.LastGetAdsLocation;
import static xorrow.mrfixer.Utils.DataHandler.getDataHandlerInstance;
import static xorrow.mrfixer.Activities.MainActivity.mMap;


/**
 * Created by Asad on 14-Jan-17.
 */

public class ApplicationClass extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static FirebaseAuth mAuth;
    public static FirebaseUser firebaseUser;
    public static FirebaseAuth.AuthStateListener mAuthListener;
    public static FirebaseDatabase database;
    public static DatabaseReference databaseReference;
    public static String UID;
    public static GoogleApiClient mGoogleApiClient;
    public static LocationRequest mLocationRequest;
    public static Location mLastLocation;
    public static Location CurrentLocation = new Location("");
//    public static int searchRadius;


    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();

        CurrentLocation = new Location("");
        CurrentLocation.setLatitude(0.0);
        CurrentLocation.setLongitude(0.0);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                Log.d("ApplicationClass", "onAuthStateChanged");
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    UID = firebaseUser.getUid();
                    Log.d("ApplicationClass", "onAuthStateChanged:signed_in:" + firebaseUser.getUid());
                } else {
                    Log.d("ApplicationClass", "onAuthStateChanged:signed_out");
                }
            }
        };
        mAuth.addAuthStateListener(mAuthListener);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(DEBUG_TAG, "onConnected,function called");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            Log.d(DEBUG_TAG, "checkSelfPermission,returning");

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //Log.v("Returning from permissions check", "YES");
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);


        if (mLastLocation != null) {
            Log.d(DEBUG_TAG, "LastLocation:" + mLastLocation.toString());

            CurrentLocation = mLastLocation;
            animateToCurrentLoc();
//            Log.d("LastLocation", mLastLocation.toString());
        }
        startLocationUpdates();
    }

    private synchronized void startLocationUpdates() {
        Log.d(DEBUG_TAG, "startLocationUpdates,function called");

        // Create the location request
        long FASTEST_INTERVAL = 2000;
        long UPDATE_INTERVAL = 10000;
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Log.d(DEBUG_TAG, "FusedLocationApi, requestLocationUpdates");

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();

    }


    public static void connectGoogleApiClient() {
        if (!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
        Log.d(DEBUG_TAG, "connectGoogleApiClient,function called");

    }

    public static void disconnectGoogleApiClient() {
        mGoogleApiClient.disconnect();

    }

    public static void animateToCurrentLoc() {
        Log.d(DEBUG_TAG, "animateToCurrentLoc,function called");

        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(CurrentLocation.getLatitude(), CurrentLocation.getLongitude()), 12f);
        mMap.animateCamera(cu);

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(DEBUG_TAG, "onLocationChanged" + location.toString());
        CurrentLocation = location;

        if (LastGetAdsLocation == null || CurrentLocation.distanceTo(LastGetAdsLocation) > 1000) {
            LastGetAdsLocation = CurrentLocation;
            getDataHandlerInstance().getNearbyusers(location.getLatitude(), location.getLongitude());
        }
    }


}
