package xorrow.mrfixer.Utils;

import android.location.Location;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import xorrow.mrfixer.DataModels.WorkerDataModel;
import xorrow.mrfixer.R;

import static xorrow.mrfixer.Application.ApplicationClass.CurrentLocation;
import static xorrow.mrfixer.Application.ApplicationClass.databaseReference;
import static xorrow.mrfixer.Application.Constants.DEBUG_TAG;
import static xorrow.mrfixer.Application.Constants.SEARCH_RADIUS;
import static xorrow.mrfixer.Activities.MainActivity.mMap;

/**
 * Created by Asad on 18-Jan-17.
 */

public class DataHandler {

    private static DataHandler instance = null;
    public ArrayList<String> listOfWorkersID = new ArrayList<>(); //workers uid
    public Map<String, LatLng> WorkersLocation = new HashMap<>(); //worker uid to its location mapping
    public ArrayList<WorkerDataModel> listOfWorkers = new ArrayList<>(); //list of workerDataModel objects
    public Map<String, WorkerDataModel> WorkerIDtoWorkerObjMap = new HashMap<>();
    public static Location LastGetAdsLocation;


    private DataHandler() {
    }

    public static DataHandler getDataHandlerInstance() {
        if (instance == null)
            instance = new DataHandler();

        return instance;
    }

    public void getNearbyusers(double lat, double lng) {


        listOfWorkersID.clear();
        WorkersLocation.clear();
        listOfWorkers.clear();
        WorkerIDtoWorkerObjMap.clear();

        GeoFire geofireRef = new GeoFire(FirebaseDatabase.getInstance().getReference("GeoLocation"));
        GeoQuery geoQuery = geofireRef.queryAtLocation(new GeoLocation(lat, lng), SEARCH_RADIUS);
        Log.d(DEBUG_TAG, "geoQuery with radius " + SEARCH_RADIUS);
        if (mMap != null)
            mMap.clear();
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {

                Log.d(DEBUG_TAG, key + ":" + location.toString());

                if (!listOfWorkersID.contains(key)) {
                    listOfWorkersID.add(key);
                }

                WorkersLocation.put(key, new LatLng(location.latitude, location.longitude));
                getWorkerData(key);
            }

            @Override
            public void onKeyExited(String key) {
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
            }


            @Override
            public void onGeoQueryReady() {

                Log.d(DEBUG_TAG, "onGeoQueryReady");

//                for (int i = 0; i < listOfWorkersID.size(); i++) {
//                    getWorkerData(i);
//                }


            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                Log.v("GeofireError", error.toString());
            }
        });


    }

    private void getWorkerData(final String UID) {


//        Log.v("DataofRetailer", uid);
        DatabaseReference ref = databaseReference.child("Retailers").child(UID);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                WorkerDataModel worker = dataSnapshot.getValue(WorkerDataModel.class);
//                Log.d(DEBUG_TAG, "getWorkerData:" + UID + "--" + dataSnapshot.toString());

                if (worker != null) {
                    worker.setDistance(getDistance(WorkersLocation.get(UID)));

                    listOfWorkers.add(worker);
                    WorkerIDtoWorkerObjMap.put(UID, worker);

                    BitmapDescriptor icon;
                    icon = BitmapDescriptorFactory.fromResource(R.drawable.screwhomemap);


                    if (worker != null && worker.getProfession() != null) {
                        if (worker.getProfession().toLowerCase()
                                .contains("plumber")) {
                            icon = BitmapDescriptorFactory.fromResource(R.drawable.screwhomemap);
                        } else if (worker.getProfession().toLowerCase()
                                .contains("electrician")) {
                            icon = BitmapDescriptorFactory.fromResource(R.drawable.bolthome);
                        } else if (worker.getProfession().toLowerCase()
                                .contains("carpanter")) {
                            icon = BitmapDescriptorFactory.fromResource(R.drawable.sawmap);
                        } else if (worker.getProfession().toLowerCase()
                                .contains("mechanic")) {
                            icon = BitmapDescriptorFactory.fromResource(R.drawable.mechanichome);
                        } else
                            icon = BitmapDescriptorFactory.fromResource(R.drawable.screwhomemap);
                    }

                    if (WorkersLocation.get(UID) != null) {
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(WorkersLocation.get(UID).latitude, WorkersLocation.get(UID).longitude))
                                .title(worker != null ? worker.getName() : "")
                                .snippet(worker != null ? worker.getProfession() : "")
                                .icon(icon)
                        );
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public Double getDistance(LatLng locationObj) {
        int R = 6371; // km (Earth radius)
        Double lat = locationObj.latitude;
        Double currentLat = CurrentLocation.getLatitude();
        Double lng = locationObj.longitude;
        Double currentLong = CurrentLocation.getLongitude();

        if (lat == null || lng == null)
            return null;

        Double dLat = toRadians(lat - currentLat);
        Double dLon = toRadians(lng - currentLong);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(toRadians(lat)) * Math.cos(toRadians(lat));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//        Log.d("getDistance", String.valueOf(Math.ceil(R * c * 0.62137)));
//        Double result = R * c * 0.62137;
        Double result = R * c;
//        if (result < 2)
//            return result;
        return Math.ceil(result);

    }

    private Double toRadians(Double deg) {
        return deg * (Math.PI / 180);
    }

    public static class distanceComparator implements Comparator<WorkerDataModel> {
        public int compare(WorkerDataModel left, WorkerDataModel right) {
            return (int) (left.getDistance() - right.getDistance());
        }

    }

    public List<WorkerDataModel> getWorkersList() {
        List<WorkerDataModel> temp = new ArrayList<>();
        temp.addAll(listOfWorkers);
        Collections.sort(temp, new distanceComparator());
        return temp;
    }


}
