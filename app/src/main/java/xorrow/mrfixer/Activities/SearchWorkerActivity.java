package xorrow.mrfixer.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

import xorrow.mrfixer.R;
import xorrow.mrfixer.Adapters.WorkerDataAdapter;
import xorrow.mrfixer.DataModels.WorkerDataModel;

import static xorrow.mrfixer.Application.Constants.DEBUG_TAG;
import static xorrow.mrfixer.Utils.DataHandler.getDataHandlerInstance;

public class SearchWorkerActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    WorkerDataAdapter adapter;
    FloatingSearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_worker);


        searchView = (FloatingSearchView) findViewById(R.id.floating_search_view);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new WorkerDataAdapter(R.layout.worker_layout, getDataHandlerInstance().getWorkersList(), SearchWorkerActivity.this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                Log.d(DEBUG_TAG, oldQuery + ":" + newQuery);
                filter(newQuery);
            }
        });

        searchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {
                finish();
            }
        });

        recyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter baseQuickAdapter, View view, final int i) {
//                Intent intent = new Intent(SearchWorkerActivity.this, UserProfile.class);
                String name = getDataHandlerInstance().getWorkersList().get(i).getName();
                String profession = getDataHandlerInstance().getWorkersList().get(i).getProfession();
                String ratings = ((TextView) view.findViewById(R.id.starRatings)).getText().toString();
//                intent.putExtra("name", name);
//                intent.putExtra("profession", profession);
//                intent.putExtra("ratings", ratings);
//                intent.putExtra("distance", getDataHandlerInstance().getWorkersList().get(i).getDistance().toString());
//
//                startActivity(intent);

                final Dialog dialog = new Dialog(SearchWorkerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.worker_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                //you can dynamically change dialog view ki cheezein by first initializing variable and
                //then using dialog.setText or dialog.setImage


                ImageView callWorker;
                ImageView messageWorker;
                ImageView closeDialog;

                callWorker = (ImageView) dialog.findViewById(R.id.call_worker);
                messageWorker = (ImageView) dialog.findViewById(R.id.message_worker);
                closeDialog = (ImageView) dialog.findViewById(R.id.exit_dialog);


                ImageView ImageWorkerDialog = (ImageView) dialog.findViewById(R.id.dialog_image_worker);
                TextView workerName = (TextView) dialog.findViewById(R.id.workerName_dialog);
                TextView workerProfession = (TextView) dialog.findViewById(R.id.workerProfession_dialog);
                workerName.setText(name);
                workerProfession.setText(profession);
                Drawable workerImage;

                if (profession.toLowerCase()
                        .contains("plumber")) {
                    workerImage = ContextCompat.getDrawable(SearchWorkerActivity.this, R.drawable.screwhomemap);
                } else if (profession.toLowerCase()
                        .contains("electrician")) {
                    workerImage = ContextCompat.getDrawable(SearchWorkerActivity.this, R.drawable.bolthome);
                } else if (profession.toLowerCase()
                        .contains("carpanter")) {
                    workerImage = ContextCompat.getDrawable(SearchWorkerActivity.this, R.drawable.sawmap);
                } else if (profession.toLowerCase()
                        .contains("mechanic")) {
                    workerImage = ContextCompat.getDrawable(SearchWorkerActivity.this, R.drawable.mechanichome);
                } else
                    workerImage = ContextCompat.getDrawable(SearchWorkerActivity.this, R.drawable.screwhomemap);

                ImageWorkerDialog.setImageDrawable(workerImage);


                callWorker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + getDataHandlerInstance().getWorkersList().get(i).getPhoneNumber()));

                        Dexter.withActivity(SearchWorkerActivity.this)
                                .withPermission(Manifest.permission.CALL_PHONE)

                                .withListener(new PermissionListener() {
                                    @Override
                                    public void onPermissionGranted(PermissionGrantedResponse response) {
                                        Log.d(DEBUG_TAG, "PermissionGranted");
                                        SearchWorkerActivity.this.startActivity(callIntent);


                                    }

                                    @Override
                                    public void onPermissionDenied(PermissionDeniedResponse response) {
                                        Log.d(DEBUG_TAG, "PermissionDenied,asking for permission");
                                        Toast.makeText(SearchWorkerActivity.this, "No permission granted please grant permission to avail the call service", Toast.LENGTH_SHORT).show();


                                    }

                                    @Override
                                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                        token.continuePermissionRequest();

                                    }
                                })
                                .onSameThread()
                                .check();

//                        onSendNotificationsButtonClick(v);

                    }
                });

                messageWorker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent smsIntent = new Intent(Intent.ACTION_SENDTO,
                                Uri.parse("sms:" + getDataHandlerInstance().getWorkersList().get(i).getPhoneNumber()));
                        smsIntent.putExtra("sms_body", "Hi i see your profile on Mr Fixer i want to hire you for work.");

                        SearchWorkerActivity.this.startActivity(smsIntent);

//                        onSendNotificationsButtonClick(v);
                    }
                });

                closeDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });

    }


    public void filter(String text) {

        List<WorkerDataModel> temp = new ArrayList<>();
//        temp.addAll(getDataHandlerInstance().getWorkersList());

        text = text.toLowerCase();
        for (WorkerDataModel worker : getDataHandlerInstance().getWorkersList()) {
            if (worker.getProfession().toLowerCase().contains(text) || worker.getName().toLowerCase().contains(text)) {
                temp.add(worker);
            }
        }

        adapter.setNewData(temp);
        adapter.notifyDataSetChanged();
    }


}
