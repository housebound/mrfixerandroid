package xorrow.mrfixer.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import xorrow.mrfixer.R;
import xorrow.mrfixer.Utils.NavigationDrawerSetup;

import static xorrow.mrfixer.Application.ApplicationClass.animateToCurrentLoc;
import static xorrow.mrfixer.Application.ApplicationClass.connectGoogleApiClient;
import static xorrow.mrfixer.Application.ApplicationClass.disconnectGoogleApiClient;
import static xorrow.mrfixer.Application.Constants.DEBUG_TAG;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private NavigationDrawerSetup navigationDrawerSetup;
    private CrystalSeekbar searchRadiusSeekbar;
    private TextView searchRadiusText;
    public static GoogleMap mMap;
    public Button findWorker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(xorrow.mrfixer.R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Mr. Fixer");
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        searchRadiusText = (TextView) findViewById(R.id.search_radius_text);
        searchRadiusSeekbar = (CrystalSeekbar) findViewById(R.id.search_radius_seekbar);
        navigationDrawerSetup = new NavigationDrawerSetup(drawerLayout, toolbar, navigationView, this, searchRadiusSeekbar, searchRadiusText);


        findWorker = (Button) findViewById(R.id.find_worker);
        findWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isNetworkAvailable()) {
                    Toast.makeText(MainActivity.this, "No connectivity. Please check your internet connection.", Toast.LENGTH_SHORT).show();
                } else
                    startActivity(new Intent(MainActivity.this, SearchWorkerActivity.class));

            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void buildAlertMessageNoGps() {
        new MaterialDialog.Builder(MainActivity.this)
                .theme(Theme.LIGHT)
                .content("Your GPS seems to be turned off. Please turn on your GPS.")
                .canceledOnTouchOutside(false)
                .positiveText("Location Settings")
                .positiveColorRes(R.color.colorPrimary)
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                    }
                })
                .show();
    }


    public void onStart() {
        super.onStart();
//        Log.d("onStart", "connectGoogleApiClient");
//        connectGoogleApiClient();

        navigationDrawerSetup.ConfigureDrawer();
//        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            buildAlertMessageNoGps();
//        }

        Dexter.withActivity(MainActivity.this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)

                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Log.d(DEBUG_TAG, "PermissionGranted");
                        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            buildAlertMessageNoGps();
                        } else
                            initializeMap();

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Log.d(DEBUG_TAG, "PermissionDenied,asking for permission");

                        new MaterialDialog.Builder(MainActivity.this)
                                .theme(Theme.LIGHT)

                                .title("Location Permission")
                                .content("Location access is needed to track your current location")
                                .positiveText("Grant Permission")
                                .positiveColorRes(R.color.colorPrimary)

                                .cancelable(false)
                                .canceledOnTouchOutside(false)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Context context = getApplicationContext();
                                        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.parse("package:" + context.getPackageName()));
                                        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                                        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(myAppSettings);
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();

                    }
                })
                .onSameThread()
                .check();

    }


    public void initializeMap() {
        Log.d(DEBUG_TAG, "initializeMap Called");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_map);

        mapFragment.getMapAsync(this);
    }

    public void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(0).setChecked(true); //select home by default in navigation drawer
    }

    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Log.v("OnDestroy called", "HomeActivity");
        disconnectGoogleApiClient();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(DEBUG_TAG, "onMapReady function called");
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//
//            return;
//        }
        mMap.setMyLocationEnabled(true);
        connectGoogleApiClient();

//        mMap.setPadding(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics()));

        Handler handler = new Handler();
        Log.d(DEBUG_TAG, "onMapReady, calling animateToCurrentLoc after 3 seconds");

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(DEBUG_TAG, "onMapReady, calling animateToCurrentLoc now");

                animateToCurrentLoc();
            }
        }, 3000);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            switch (requestCode) {
                case 0:
                    if (mMap == null)
                        initializeMap();

                    break;
            }
        }
    }
}
